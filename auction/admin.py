from django.contrib import admin
from auction.models import AuctionObject, AuctionObjectImage, Car, Assorted, Profile, Offer
# Register your models here.

# admin.py
class AuctionObjectImageInline(admin.TabularInline):
    model = AuctionObjectImage
    extra = 10

class AuctionObjectAdmin(admin.ModelAdmin):
    inlines = [ AuctionObjectImageInline, ]

    list_display = ('title', 'current_offer', 'description',)

class CarAdmin(admin.ModelAdmin):
    inlines = [AuctionObjectImageInline, ]

    list_display = ('title', 'current_offer', 'description',)

class AssortedAdmin(admin.ModelAdmin):
    inlines = [AuctionObjectImageInline, ]

    list_display = ('title', 'current_offer', 'description',)

class ProfileAdmin(admin.ModelAdmin):

    list_display = ('user', 'social_id')

class OfferAdmin(admin.ModelAdmin):

    list_display = ('user', 'offer')

admin.site.register(AuctionObject, AuctionObjectAdmin)
admin.site.register(Car, CarAdmin)
admin.site.register(Assorted, AssortedAdmin)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(Offer, OfferAdmin)