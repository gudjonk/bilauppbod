# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AuctionObject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Skradur')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Breytt')),
                ('time_auction_expires', models.DateTimeField(verbose_name='Rennur ut')),
                ('price', models.IntegerField(verbose_name='Verd')),
                ('minimum_price', models.IntegerField(default=0, verbose_name='Lagmarksbod')),
                ('minimum_offer_raise', models.IntegerField(default=5000, verbose_name='Lagmarks haekkun')),
                ('display_photo', models.ImageField(upload_to=None, verbose_name='Adalmynd')),
                ('title', models.CharField(max_length=100, verbose_name='Titill')),
                ('description', models.TextField(default='', verbose_name='Lysing', blank=True)),
                ('resume', models.FileField(upload_to=None, null=True, verbose_name='Ferilskra', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='AuctionObjectImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Offer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Skradur')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Breytt')),
                ('offer', models.IntegerField(verbose_name='Bod')),
            ],
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('social_id', models.CharField(max_length=10, verbose_name='Kennitala')),
                ('name', models.CharField(max_length='100', verbose_name='Nafn')),
                ('address', models.CharField(max_length='100', verbose_name='Heimilisfang')),
                ('postal_code', models.CharField(default='', max_length='10', verbose_name='Potfang', blank=True)),
                ('place', models.CharField(default='', max_length='20', verbose_name='Stadur', blank=True)),
                ('cell_phone', models.CharField(default='', max_length='20', verbose_name='Farsimi')),
                ('home_phone', models.CharField(default='', max_length='20', verbose_name='Heimasimi', blank=True)),
                ('work_phone', models.CharField(default='', max_length='20', verbose_name='Vinnusimi', blank=True)),
                ('subscribed_to_newsletter', models.BooleanField(default=True, verbose_name='Vill fa frettabref')),
                ('ip_number', models.CharField(default='', max_length='50', verbose_name='', blank=True)),
                ('user', models.ForeignKey(verbose_name='Notandi', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Assorted',
            fields=[
                ('auctionobject_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='auction.AuctionObject')),
            ],
            bases=('auction.auctionobject',),
        ),
        migrations.CreateModel(
            name='Car',
            fields=[
                ('auctionobject_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='auction.AuctionObject')),
                ('maker', models.CharField(max_length=30, verbose_name='Framleidandi')),
                ('year', models.CharField(default='', max_length=10, verbose_name='Argerd', blank=True)),
                ('registered', models.DateField(verbose_name='Fyrsti skraningardagur')),
                ('newly_registered', models.DateField(verbose_name='Nyskradur')),
                ('registry_number', models.CharField(max_length=20, verbose_name='Fastanumer')),
                ('kilometer_count', models.FloatField(verbose_name='Akstur')),
                ('written_off_registry', models.BooleanField(default=False, verbose_name='Skrad tjonaokutaeki')),
                ('seller', models.CharField(max_length=50, verbose_name='Seljandi')),
                ('color', models.CharField(default='', max_length=20, verbose_name='Litur', blank=True)),
                ('gears', models.IntegerField(default=5, verbose_name='Girar')),
                ('transmission', models.CharField(default='automatic', max_length=20, verbose_name='Skipting', choices=[('manual', 'Beinsk.'), ('automatic', 'Sjalfsk.')])),
                ('number_of_doors', models.IntegerField(default=4, verbose_name='Fjoldi hurda', blank=True)),
                ('engine_type', models.CharField(max_length=30, verbose_name='Velargerd (eldsneyti)', choices=[('gasoline', 'Bensin'), ('diesel', 'Disel'), ('electric', 'Rafmagn'), ('hydrogen', 'Vetni')])),
                ('engine_size', models.IntegerField(verbose_name='Velarstaerd (slagrymi)')),
            ],
            bases=('auction.auctionobject',),
        ),
        migrations.AddField(
            model_name='offer',
            name='auction_object',
            field=models.ForeignKey(verbose_name='Hlutur', to='auction.AuctionObject'),
        ),
        migrations.AddField(
            model_name='offer',
            name='user',
            field=models.ForeignKey(related_name='offerer', verbose_name='Bjodandi', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='auctionobjectimage',
            name='auction_object',
            field=models.ForeignKey(related_name='images', to='auction.AuctionObject'),
        ),
        migrations.AddField(
            model_name='auctionobject',
            name='current_offer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, default=None, blank=True, to='auction.Offer', null=True, verbose_name='Nuverandi bod'),
        ),
    ]
