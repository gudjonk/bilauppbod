# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auction', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='offer',
            name='on_offer_watch',
            field=models.BooleanField(default=False, verbose_name='Tilkynna ef bo\xf0 fellur \xfar gildi'),
        ),
        migrations.AlterField(
            model_name='auctionobject',
            name='display_photo',
            field=models.ImageField(upload_to=b'media/auction_images', verbose_name='A\xf0almynd'),
        ),
    ]
