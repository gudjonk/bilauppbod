# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auction', '0002_auto_20180304_1425'),
    ]

    operations = [
        migrations.AddField(
            model_name='auctionobjectimage',
            name='description',
            field=models.TextField(default=b'', verbose_name='L\xfdsing', blank=True),
        ),
        migrations.AddField(
            model_name='auctionobjectimage',
            name='image',
            field=models.ImageField(upload_to=b'media/auction_images', null=True, verbose_name='Mynd', blank=True),
        ),
        migrations.AddField(
            model_name='auctionobjectimage',
            name='title',
            field=models.TextField(default=b'', verbose_name='Titill', blank=True),
        ),
        migrations.AddField(
            model_name='car',
            name='damaged',
            field=models.BooleanField(default=True, verbose_name='Skemmdur'),
        ),
        migrations.AddField(
            model_name='offer',
            name='accepted',
            field=models.NullBooleanField(default=None, verbose_name='Sam\xfeykkt'),
        ),
    ]
