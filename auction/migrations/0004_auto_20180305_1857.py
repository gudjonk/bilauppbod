# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auction', '0003_auto_20180304_2310'),
    ]

    operations = [
        migrations.AddField(
            model_name='auctionobject',
            name='auctioned',
            field=models.BooleanField(default=False, verbose_name='Seldur'),
        ),
        migrations.AlterField(
            model_name='offer',
            name='auction_object',
            field=models.ForeignKey(related_name='offers', verbose_name='Hlutur', to='auction.AuctionObject'),
        ),
    ]
