# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auction', '0004_auto_20180305_1857'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='banned',
            field=models.BooleanField(default=False, verbose_name='Notandi banna\xf0ur'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='ip_number',
            field=models.CharField(default='', max_length='50', verbose_name='IP tala', blank=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='user',
            field=models.ForeignKey(related_name='profile', verbose_name='Notandi', to=settings.AUTH_USER_MODEL),
        ),
    ]
