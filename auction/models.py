# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.conf import settings

import locale
locale.setlocale(locale.LC_ALL, '')

class AuctionObject(models.Model):
    created = models.DateTimeField(verbose_name=u'Skráð', auto_now=False, auto_now_add=True, blank=False, null=False)
    modified = models.DateTimeField(verbose_name=u'Breytt', auto_now=True, blank=False, null=False)

    time_auction_expires = models.DateTimeField(verbose_name=u'Rennur út', blank=False, null=False)

    price = models.IntegerField(verbose_name=u'Verð', blank=True, null=True, default=None)
    minimum_price = models.IntegerField(verbose_name=u'Lágmarksboð', blank=False, null=False, default=0)
    minimum_offer_raise = models.IntegerField(verbose_name=u'Lágmarks hækkun', blank=False, null=False, default=5000, )
    current_offer = models.ForeignKey('Offer', verbose_name=u'Núverandi boð', blank=True, null=True, default=None,
                                      on_delete=models.SET_NULL)

    display_photo = models.ImageField(verbose_name=u'Aðalmynd', upload_to='media/auction_images', blank=False, )
    title = models.CharField(verbose_name=u'Titill', blank=False, null=False, max_length=100)
    description = models.TextField(verbose_name=u'Lýsing', blank=True, null=False, default='')

    resume = models.FileField(verbose_name=u'Ferilskrá', upload_to='media/pdf', blank=True, null=True)

    buy_now = models.BooleanField(verbose_name=u'Kaupa núna takki', blank=False, null=False, default=False)

    auctioned = models.BooleanField(verbose_name=u'Seldur', blank=False, null=False, default=False)

    def get_auction_time_left(self):
        print(self.time_auction_expires)
        if self.time_auction_expires < timezone.now():
            return 'Útrunnið'
        time_left = self.time_auction_expires - timezone.now()
        print(time_left.total_seconds(), time_left.total_seconds()//3600)
        if time_left.total_seconds()//3600 >= 48:
            return '{days} dagar og {hours} klst eftir'.format(days=int(time_left.days), hours=int((time_left.total_seconds()//3600)%24))
        elif time_left.total_seconds()//3600 >= 24:
            return '{day} dagur og {hours} klst eftir'.format(day=int(time_left.days), hours=int((time_left.total_seconds()//3600)%24))
        return '{hours} klst og {minutes} mínútur eftir'.format(hours=int(time_left.total_seconds()//3600), minutes=int((time_left.total_seconds()//60)%60))

    def is_minimum_price_reached(self):
        return self.current_offer.offer >= self.minimum_price if self.current_offer and self.minimum_price else False

    def is_auction_expired(self):
        return self.time_auction_expires <= timezone.now()

    def get_formatted_offer(self):
        if not self.current_offer:
            return 'Ekkert boð'
        return locale.format('%d', self.current_offer.offer, 1) + ' kr.'

    def get_formatted_minimum_offer(self):
        return locale.format('%d', self.minimum_price, 1) + ' kr.'


class AuctionObjectImage(models.Model):
    auction_object = models.ForeignKey(AuctionObject, blank=False, null=False, related_name='images')

    image = models.ImageField(verbose_name=u'Mynd', upload_to='media/auction_images', blank=True, null=True, )
    title = models.TextField(verbose_name=u'Titill', blank=True, null=False, default='', )
    description = models.TextField(verbose_name=u'Lýsing', blank=True, null=False, default='', )


class Car(AuctionObject):
    MANUAL_SHIFT = 'manual'
    AUTOMATIC_SHIFT = 'automatic'
    TRANSMISSION_CHOICES = (
        (MANUAL_SHIFT, 'Beinsk.'),
        (AUTOMATIC_SHIFT, 'Sjálfsk.'),
    )

    GASOLINE = 'gasoline'
    DIESEL = 'diesel'
    ELECTRIC = 'electric'
    HYDROGEN = 'hydrogen'
    FUEL_TYPE_CHOICES = (
        (GASOLINE, 'Bensín'),
        (DIESEL, 'Dísel'),
        (ELECTRIC, 'Rafmagn'),
        (HYDROGEN, 'Vetni'),
    )

    maker = models.CharField(verbose_name=u'Framleiðandi', blank=False, null=False, max_length=30)
    year = models.CharField(verbose_name=u'Árgerð', blank=True, null=False, default='', max_length=10)

    registered = models.DateField(verbose_name=u'Fyrsti skráningardagur', blank=False, null=False,)
    newly_registered = models.DateField(verbose_name=u'Nýskráður', blank=False, null=False)
    registry_number = models.CharField(verbose_name=u'Fastanúmer', max_length=20, blank=False, null=False)
    kilometer_count = models.FloatField(verbose_name=u'Akstur', blank=False, null=False)
    written_off_registry = models.BooleanField(verbose_name=u'Skráð tjónaökutæki', blank=False, default=False,
                                               null=False)
    damaged = models.BooleanField(verbose_name=u'Skemmdur', blank=False, default=True, null=False)
    seller = models.CharField(verbose_name=u'Seljandi', blank=False, null=False, max_length=50)

    color = models.CharField(verbose_name=u'Litur', blank=True, null=False, default='', max_length=20)
    gears = models.IntegerField(verbose_name=u'Gírar', blank=False, null=False, default=5)
    transmission = models.CharField(verbose_name=u'Skipting', blank=False, null=False, max_length=20,
                                    default=AUTOMATIC_SHIFT, choices=TRANSMISSION_CHOICES)
    number_of_doors = models.IntegerField(verbose_name=u'Fjöldi hurða', blank=True, null=False, default=4)
    engine_type = models.CharField(verbose_name=u'Vélargerð (eldsneyti)', blank=False, null=False,
                                   choices=FUEL_TYPE_CHOICES, max_length=30)
    engine_size = models.IntegerField(verbose_name=u'Vélarstærð (slagrými)', blank=False, null=False, )

    def get_formatted_km_count(self):
        return locale.format('%d', self.kilometer_count, 1) + ' km'

    def get_formatted_cc_count(self):
        locale.format('%d', self.engine_size, 1)

class Assorted(AuctionObject):
    pass


class Offer(models.Model):
    created = models.DateTimeField(verbose_name=u'Skráð', auto_now=False, auto_now_add=True, blank=False, null=False)
    modified = models.DateTimeField(verbose_name=u'Breytt', auto_now=True, blank=False, null=False)

    user = models.ForeignKey(User, verbose_name=u'Bjóðandi', blank=False, null=False, on_delete=models.CASCADE,
                             related_name="offerer")
    auction_object = models.ForeignKey(AuctionObject, verbose_name=u'Hlutur', blank=False, null=False,
                                       on_delete=models.CASCADE, related_name='offers')

    on_offer_watch = models.BooleanField(verbose_name=u'Tilkynna ef boð fellur úr gildi', blank=False, null=False,
                                         default=False)

    offer = models.IntegerField(verbose_name=u'Boð', blank=False, null=False)
    accepted = models.NullBooleanField(verbose_name=u'Samþykkt', blank=True, null=True, default=None)


    def get_formatted_offer(self):
        print(self.offer)
        return locale.format('%d', self.offer, 1) + ' kr.'


class Profile(models.Model):
    user = models.ForeignKey(User, verbose_name=u'Notandi', blank=False, null=False, on_delete=models.CASCADE, related_name='profile')

    social_id = models.CharField(verbose_name=u'Kennitala', blank=False, null=False, max_length=10, )
    name = models.CharField(verbose_name=u'Nafn', blank=False, null=False, max_length=100)
    address = models.CharField(verbose_name=u'Heimilisfang', blank=False, null=False, max_length=100)
    postal_code = models.CharField(verbose_name=u'Póstfang', blank=True, null=False, default='', max_length=10)
    place = models.CharField(verbose_name=u'Staður', blank=True, null=False, default='', max_length=20)
    cell_phone = models.CharField(verbose_name=u'Farsími', blank=False, null=False, default='', max_length=20)
    home_phone = models.CharField(verbose_name=u'Heimasími', blank=True, null=False, default='', max_length=20)
    work_phone = models.CharField(verbose_name=u'Vinnusími', blank=True, null=False, default='', max_length=20)

    subscribed_to_newsletter = models.BooleanField(verbose_name=u'Vill fá fréttabréf', blank=False, null=False,
                                                   default=True)
    ip_number = models.CharField(verbose_name=u'IP tala', blank=True, null=False, default='', max_length=50)

    banned = models.BooleanField(verbose_name=u'Notandi bannaður', blank=False, null=False, default=False)

@receiver(post_save, sender=User    )
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
