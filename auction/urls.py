import debug_toolbar
from django.conf.urls import url, include
from django.views.generic import RedirectView
from auction.views import AuctionItemView, AuctionItemOfferView
from pages.frontendViews import WaitingOffersView, WaitingOffersOthersView

urlpatterns = [
    url(r'^item/(?P<pk>[0-9]+)/$', AuctionItemView.as_view()),  # frontEndViews #any
    url(r'^item/(?P<pk>[0-9]+)/offer/$', AuctionItemOfferView.as_view()),  # frontEndViews #any
    url(r'^waiting/$', WaitingOffersView.as_view()),
    url(r'^waiting/(?P<pk>[0-9]+)/$', WaitingOffersOthersView.as_view()),
]