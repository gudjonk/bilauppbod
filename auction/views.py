# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from django.http import HttpResponseForbidden
from pages.utils import prepare_http_response
from pages.mailutils import sendcontact, sendsignupreceived, send_signup_request_notification_email, notify_offer_watch

from auction.models import AuctionObject, Car, Assorted, Offer
from datetime import datetime
from django.shortcuts import redirect

class AuctionItemView(TemplateView):
    d = dict()
    template_name = "auction/auction_item.html"
    def get(self, request, *args, **kwargs):
        self.d['item'] = Car.objects.filter(pk=self.kwargs['pk'])
        self.d['item'] = self.d['item'].first() if self.d['item'].exists() else Assorted.objects.get(pk=self.kwargs['pk'])
        return prepare_http_response(self.d, request, self.template_name)

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return generic.View.dispatch(self, request, *args, **kwargs)

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        return prepare_http_response(self.d, request, self.template_name)


class AuctionItemOfferView(TemplateView):
    d = dict()
    template_name = "auction/auction_item_offer.html"
    def get(self, request, *args, **kwargs):
        self.d['item'] = Car.objects.filter(pk=self.kwargs['pk'])
        self.d['item'] = self.d['item'].first() if self.d['item'].exists() else Assorted.objects.get(pk=self.kwargs['pk'])
        return prepare_http_response(self.d, request, self.template_name)

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return generic.View.dispatch(self, request, *args, **kwargs)

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        self.d['item'] = Car.objects.filter(pk=self.kwargs['pk'])
        self.d['item'] = self.d['item'].first() if self.d['item'].exists() else Assorted.objects.get(
            pk=self.kwargs['pk'])
        if not request.user.is_authenticated():
            return HttpResponseForbidden()

        terms_and_conditions = request.POST.get('terms_and_conditions', False)
        terms_and_conditions = True if terms_and_conditions == u'on' else False
        self.d['action'] = request.POST.get('action')

        if self.d['item'].buy_now and self.d['action'] == 'buy_now':
            new_offer = self.d['item'].minimum_price
            current_offer = 0
            on_offer_watch = False
            self.d['item'].time_auction_expires = datetime.now()
        else:
            new_offer = int(request.POST.get('offer', '0'))
            current_offer = self.d['item'].current_offer.offer if self.d['item'].current_offer else 0
            on_offer_watch = request.POST.get('auction_watch', False)
            on_offer_watch = True if on_offer_watch == u'on' else False

        if not terms_and_conditions:
            self.d['Message'] = 'Skilmálar verða að vera samþykktir'
            return prepare_http_response(self.d, request, self.template_name)
        if new_offer < current_offer + self.d['item'].minimum_offer_raise:
            self.d['Message'] = 'Boð of lágt, Lágmarkshækkun: {minimum_offer_raise}'.format(minimum_offer_raise=self.d['item'].minimum_offer_raise)
            return prepare_http_response(self.d, request, self.template_name)

        offer = Offer()
        offer.user = request.user
        offer.auction_object = self.d['item']
        offer.offer = new_offer
        offer.on_offer_watch = on_offer_watch
        offer.save()

        if self.d['item'].current_offer and self.d['item'].current_offer.on_offer_watch:
            notify_offer_watch(self.d['item'].current_offer)

        self.d['item'].current_offer = offer
        self.d['item'].save()

        #return prepare_http_response(self.d, request, self.template_name)
        return redirect('/account/')
