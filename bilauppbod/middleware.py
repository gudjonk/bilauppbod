from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import HttpResponseForbidden, HttpResponse, Http404, HttpResponseRedirect

from auction.models import Profile

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


class RestrictBannedUsersMiddleware(object):
    """
    A middleware that restricts staff members access to administration panels.
    """
    def process_request(self, request):
        if request.user.is_anonymous():
            return None
        if request.path.startswith('/admin/'):
            return None
        if request.user.profile.first().banned:
            return HttpResponseForbidden()
        if Profile.objects.filter(ip_number=get_client_ip(request), banned=True).exists():
            return HttpResponseForbidden()
