# -*- coding: utf-8 -*-
import json
import requests
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from django.http import HttpResponseForbidden
from itertools import chain

from pages.utils import prepare_http_response
from pages.mailutils import sendcontact, sendsignupreceived, send_signup_request_notification_email

from auction.models import Car, Assorted, Offer, AuctionObject, Profile

from datetime import datetime

def evaluate_recaptcha(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')

    data = dict()
    url = 'https://www.google.com/recaptcha/api/siteverify'
    data['response'] = request.POST.get('g-recaptcha-response', '')
    data['secret'] = '6LdQETcUAAAAAAfjtZpUESHyNR_GaK9YjLmcrML2'
    data['remoteip'] = ip
    r = requests.post(url, data=data)
    repsonse_json = json.loads(r.content.decode("utf-8"))
    return repsonse_json['success']


class Contact(TemplateView):
    def get(self, request, *args, **kwargs):
        d = dict()
        #d['parkingLots'] = ParkingLot.objects.filter().all()
        template_name = "pages/_contact_page.html"
        return prepare_http_response(d, request, template_name)

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return generic.View.dispatch(self, request, *args, **kwargs)

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        d = dict()
        captcha_passed = evaluate_recaptcha(request)
        d['name'] = request.POST.get('name')
        d['email'] = request.POST.get('email')
        d['phone'] = request.POST.get('phone')
        d['message'] = request.POST.get('message')

        if (captcha_passed):
            d['message_sent'] = True
            sendcontact(d)

        template_name = 'pages/_contact_page.html'
        return prepare_http_response(d, request, template_name)


class WaitingOffersView(TemplateView):
    d = dict()
    # d['parkingLots'] = ParkingLot.objects.filter().all()
    template_name = "auction/_waiting_offers.html"

    def get(self, request, *args, **kwargs):
        if request.user.is_anonymous():
            return HttpResponseForbidden()
        if not request.user.is_superuser:
            return HttpResponseForbidden()

        self.d['type'] = request.GET.get('type', 'all')
        if self.d['type'] == 'vehicles' or self.d['type'] == 'all':
            self.d['items'] = Car.objects.filter(time_auction_expires__lte=datetime.now())
        else:
            self.d['items'] = Assorted.objects.filter(time_auction_expires__lte=datetime.now())
        if not request.GET.get('auctioned'):
            self.d['items'] = self.d['items'].filter(auctioned=False)

        if self.d['type'] == 'all' and not self.d['type'] == 'assorted' and not self.d['type'] == 'vehicle':
            assorted = Assorted.objects.filter(time_auction_expires__lte=datetime.now())
            if not request.GET.get('auctioned'):
                assorted = assorted.filter(auctioned=False)
            self.d['items'] = sorted(list(chain(self.d['items'], assorted)),
                                                key=lambda instance: instance.created, reverse=True)
        ids = []
        for item in self.d['items']:
            if item.current_offer:
                ids.append(item.current_offer.id)
        self.d['offers_to_process'] = Offer.objects.filter(pk__in=ids)
        return prepare_http_response(self.d, request, self.template_name)

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return generic.View.dispatch(self, request, *args, **kwargs)

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        if request.user.is_anonymous():
            return HttpResponseForbidden()
        if not request.user.is_superuser:
            return HttpResponseForbidden()
        print(request.POST.get('pk'))
        item = AuctionObject.objects.get(pk=request.POST.get('pk'))
        if request.POST.get('action') == 'auction':
            item.auctioned = True
            item.save()
        elif request.POST.get('action') == 'ban':
            Profile.objects.filter(id=item.current_offer.user.profile.first().pk).update(banned=True)

        self.d['items'] = AuctionObject.objects.filter(auctioned=False, time_auction_expires__lte=datetime.now())
        self.d['offers_to_process'] = Offer.objects.filter(
            pk__in=self.d['items'].values_list('current_offer_id', flat=True))

        return prepare_http_response(self.d, request, self.template_name)

class WaitingOffersOthersView(TemplateView):
    d = dict()
    # d['parkingLots'] = ParkingLot.objects.filter().all()
    template_name = "auction/_waiting_offers_other.html"

    def get(self, request, *args, **kwargs):
        if request.user.is_anonymous():
            return HttpResponseForbidden()
        if not request.user.is_superuser:
            return HttpResponseForbidden()
        self.d['item'] = AuctionObject.objects.get(pk=self.kwargs['pk'])
        self.d['offers'] = Offer.objects.prefetch_related('user', 'auction_object').filter(auction_object_id=self.kwargs['pk']).order_by('-offer')
        return prepare_http_response(self.d, request, self.template_name)

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return generic.View.dispatch(self, request, *args, **kwargs)

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        return prepare_http_response(self.d, request, self.template_name)


class Signup(TemplateView):
    def get_client_ip(request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        return ip

    def make_user(self, request):
        d = dict()
        d['name'] = request.POST.get('name', '')
        d['social_id'] = request.POST.get('social_id', '')
        d['email'] = request.POST.get('email', '')
        d['phone'] = request.POST.get('phone', '')
        d['address'] = request.POST.get('address', '')
        d['postal_code'] = request.POST.get('postal_code', '')
        d['username'] = request.POST.get('username', '')
        d['password'] = request.POST.get('password', '')

        user = User()
        from auction.models import Profile
        # company_user.profile = Profile()
        # company_user.profile.social_id = signup_request.social_id.replace('-', '')
        user.first_name = d['name']
        user.email = d['email']
        user.username = d['username']
        try:
            ip = self.get_client_ip()
            user.ip_number = ip
        except Exception as e:
            pass
        user.save()

        user.set_password(d['password'])
        user.save()

        """
        social_id = models.CharField(verbose_name=u'Kennitala', blank=False, null=False, max_length=10, )
        name = models.CharField(verbose_name=u'Nafn', blank=False, null=False, max_length='100')
        address = models.CharField(verbose_name=u'Heimilisfang', blank=False, null=False, max_length='100')
        postal_code = models.CharField(verbose_name=u'Póstfang', blank=True, null=False, default='', max_length='10')
        place = models.CharField(verbose_name=u'Staður', blank=True, null=False, default='', max_length='20')
        cell_phone = models.CharField(verbose_name=u'Farsími', blank=False, null=False, default='', max_length='20')
        home_phone = models.CharField(verbose_name=u'Heimasími', blank=True, null=False, default='', max_length='20')
        work_phone = models.CharField(verbose_name=u'Vinnusími', blank=True, null=False, default='', max_length='20')
    
        subscribed_to_newsletter = models.BooleanField(verbose_name=u'Vill fá fréttabréf', blank=False, null=False,
                                                   default=True)
        """

        profile, created = Profile.objects.get_or_create(user=user)
        profile.social_id = d['social_id']
        profile.address = d['address']
        profile.postal_code = d['postal_code']
        profile.cell_phone = d['phone']
        profile.save()

    def get(self, request, *args, **kwargs):
        d = dict()
        d['signup_sent'] = False
        template_name = "pages/_signup.html"
        return prepare_http_response(d, request, template_name)

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return generic.View.dispatch(self, request, *args, **kwargs)

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        d = dict()
        captcha_passed = evaluate_recaptcha(request)
        d['company'] = request.POST.get('company', '')
        d['company_contact'] = request.POST.get('company_contact', '')
        d['company_contact_email'] = request.POST.get('company_contact_email', '')
        d['social_id'] = request.POST.get('social_id', '')
        d['message'] = request.POST.get('message', '')
        d['notification_email'] = request.POST.get('notification_email', '')
        d['signup_type'] = 'company'
        if captcha_passed:
            user = User.objects.filter(username=d['social_id'])
            if (user.exists()):
                d['user_exists'] = True
            else:
                self.make_user(request)
                mail = dict()
                mail['email'] = [d['company_contact_email'], d['notification_email']]
                sendsignupreceived(mail)
                send_signup_request_notification_email(d['social_id'])
            d['signup_sent'] = True

        template_name = "pages/_signup.html"
        return prepare_http_response(d, request, template_name)


class FrontPage(TemplateView):
    d = dict()
    template_name = "pages/_front_page.html"

    def get(self, request, *args, **kwargs):
        self.d['is_account'] = False
        self.d['time'] = request.GET.get('time', 'current')
        self.d['type'] = request.GET.get('type', 'all')
        if self.d['type'] == 'vehicles' or self.d['type'] == 'all':
            self.d['items_on_auction'] = Car.objects
        else:
            self.d['items_on_auction'] = Assorted.objects
        if self.d['time'] == 'today':
            self.d['items_on_auction'] = self.d['items_on_auction'].filter(
                time_auction_expires__range=(datetime.now(), datetime.now().replace(hour=23, minute=59, second=59))).all()
        elif self.d['time'] == 'finished':
            self.d['items_on_auction'] = self.d['items_on_auction'].filter(
                time_auction_expires__lte=datetime.now()).all()
        else:
            self.d['items_on_auction'] = self.d['items_on_auction'].filter(time_auction_expires__gte=datetime.now()).all()
        self.d['breadcrumb_string'] = ''
        if self.d['type'] == 'all':
            self.d['breadcrumb_string'] += ' - Allt'
        elif self.d['type'] == 'vehicles':
            self.d['breadcrumb_string'] += ' - Bílar'
        elif self.d['type'] == 'assorted':
            self.d['breadcrumb_string'] += ' - Ýmislegt'

        if self.d['time'] == 'current':
            self.d['breadcrumb_string'] += ' - Uppboð í gangi'
        elif self.d['time'] == 'today':
            self.d['breadcrumb_string'] += ' - Uppboð sem enda í dag'
        elif self.d['time'] == 'finished':
            self.d['breadcrumb_string'] += ' - Uppboðum lokið'

        if self.d['type'] == 'all' and not self.d['type'] == 'assorted' and not self.d['type'] == 'vehicles':
            assorted = Assorted.objects
            if self.d['time'] == 'today':
                assorted = assorted.filter(
                    time_auction_expires__range=(
                        datetime.now(), datetime.now().replace(hour=23, minute=59, second=59))).all()
            elif self.d['time'] == 'finished':
                assorted = assorted.filter(
                    time_auction_expires__lte=datetime.now()).all()
            else:
                assorted = assorted.filter(
                    time_auction_expires__gte=datetime.now()).all()
            self.d['items_on_auction'] = sorted(list(chain(self.d['items_on_auction'], assorted)), key=lambda instance: instance.created, reverse=True)

        """    
        self.d['extra_items'] = []
        if 'assorted_items' in self.d:
            for item in self.d['assorted_items']:
                if not item.id in ids:
                    self.d['extra_items'].append(item)
        """


        return prepare_http_response(self.d, request, self.template_name)

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return generic.View.dispatch(self, request, *args, **kwargs)

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        self.d['is_account'] = False
        self.d['name'] = request.POST.get('name')
        self.d['email'] = request.POST.get('email')
        self.d['phone'] = request.POST.get('phone')
        self.d['message'] = request.POST.get('message')
        print(self.d['message'])
        captcha_passed = evaluate_recaptcha(request)
        if(captcha_passed):
            self.d['message_sent'] = True
            sendcontact(self.d)
        return redirect('/contact/?submitted=1')

class AccountPage(TemplateView):
    d = dict()
    template_name = "pages/_front_page.html"

    def get(self, request, *args, **kwargs):
        self.d['is_account'] = True
        self.d['type'] = request.GET.get('type', 'all')
        self.d['time'] = request.GET.get('time', 'current')
        if self.d['type'] == 'vehicles' or self.d['type'] == 'all':
            self.d['items_on_auction'] = Car.objects
        else:
            self.d['items_on_auction'] = Assorted.objects

        self.d['offers'] = Offer.objects.filter(user_id=request.user.pk)
        self.d['offer_ids'] = self.d['offers'].values_list('id', flat=True)
        self.d['offer_item_ids'] = set(self.d['offers'].values_list('auction_object_id', flat=True))

        self.d['items_on_auction'] = self.d['items_on_auction'].filter(pk__in=self.d['offer_item_ids'])
        if self.d['time'] == 'today':
            self.d['items_on_auction'] = self.d['items_on_auction'].filter(
                time_auction_expires__range=(
                datetime.now(), datetime.now().replace(hour=23, minute=59, second=59))).all()
        elif self.d['time'] == 'finished':
            self.d['items_on_auction'] = self.d['items_on_auction'].filter(
                time_auction_expires__lte=datetime.now()).all()
        else:
            self.d['items_on_auction'] = self.d['items_on_auction'].filter(
                time_auction_expires__gte=datetime.now()).all()

        self.d['breadcrumb_string'] = ''
        if self.d['type'] == 'all':
            self.d['breadcrumb_string'] += ' - Allt'
        elif self.d['type'] == 'vehicles':
            self.d['breadcrumb_string'] += ' - Bílar'
        elif self.d['type'] == 'assorted':
            self.d['breadcrumb_string'] += ' - Ýmislegt'

        if self.d['time'] == 'current':
            self.d['breadcrumb_string'] += ' - Uppboð í gangi'
        elif self.d['time'] == 'today':
            self.d['breadcrumb_string'] += ' - Uppboð sem enda í dag'
        elif self.d['time'] == 'finished':
            self.d['breadcrumb_string'] += ' - Uppboðum lokið'


        if self.d['type'] == 'all' and not self.d['type'] == 'assorted' and not self.d['type'] == 'vehicles':
            assorted = Assorted.objects.filter(pk__in=self.d['offer_item_ids'])
            if self.d['time'] == 'today':
                assorted = assorted.filter(
                    time_auction_expires__range=(
                        datetime.now(), datetime.now().replace(hour=23, minute=59, second=59))).all()
            elif self.d['time'] == 'finished':
                assorted = assorted.filter(
                    time_auction_expires__lte=datetime.now()).all()
            else:
                assorted = assorted.filter(
                    time_auction_expires__gte=datetime.now()).all()
            self.d['items_on_auction'] = sorted(list(chain(self.d['items_on_auction'], assorted)), key=lambda instance: instance.created, reverse=True)


        return prepare_http_response(self.d, request, self.template_name)

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return generic.View.dispatch(self, request, *args, **kwargs)

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        self.d['is_account'] = True
        self.d['name'] = request.POST.get('name')
        self.d['email'] = request.POST.get('email')
        self.d['phone'] = request.POST.get('phone')
        self.d['message'] = request.POST.get('message')
        print(self.d['message'])
        captcha_passed = evaluate_recaptcha(request)
        if(captcha_passed):
            self.d['message_sent'] = True
            sendcontact(self.d)
        return redirect('/contact/?submitted=1')


class AboutPage(TemplateView):
    d = dict()
    template_name = "pages/_about.html"

    def get(self, request, *args, **kwargs):
        return prepare_http_response(self.d, request, self.template_name)

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return generic.View.dispatch(self, request, *args, **kwargs)

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        return prepare_http_response(self.d, request, self.template_name)
