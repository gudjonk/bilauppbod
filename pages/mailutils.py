# -*- coding: utf-8 -*-
from django.core.mail import send_mail
from django.template import loader

def sendcontact(info):
    html_message = '<p>Skilaboð:<br>'+info['message']+'</p>'
    html_message += '<p>Name: '+info['name']+'</p>'
    html_message += '<p>Phone: <a href="tel:'+info['phone']+'">'+info['phone']+'</a></p>'
    html_message += '<p>Email: <a href="mailto:'+info['email']+'">'+info['email']+'</a></p>'
    send_mail('Contact request: '+info['name'], 'Contact request', 'mailfrom@mailfrom.is',
              ['mailto@mailto.is'], fail_silently=False, html_message=html_message)

def sendsignupreceived(info):
    html_message = '<p>Kæri notandi</p>'
    html_message += '<br>'
    html_message += '<p>Takk fyrir skráninguna inn á Vakauppbod.is</p>'
    html_message += '<p>Með von um áframhaldandi gott samstarf.</p>'
    html_message += '<br>'
    html_message += '<p>Starfsfólk Vöku</p>'
    send_mail('Umsókn að vakauppbod', 'Umsókn að vakauppbod', 'mailfrom@mailfrom.is', info['email'], fail_silently=False, html_message=html_message)

def send_signup_request_notification_email(social_id):
    html_message = """
        <p>Signup request hefur borist</p>
        <br>
        <p>kt: """+social_id+"""</p>
        """
    send_mail('SignupRequest: Ný skráning', 'SignupRequest: Ný skráning',
              'mailfrom@mailfrom.is', ['mailto@mailto.is'], fail_silently=False,
              html_message=html_message)

def notify_offer_watch(offer):
    pass