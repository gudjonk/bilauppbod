import debug_toolbar
from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from django.contrib.auth.views import logout
from django.views.generic import RedirectView

from pages.frontendViews import Contact, Signup, FrontPage, AccountPage, WaitingOffersView, AboutPage

urlpatterns = [
    url(r'^contact', Contact.as_view()),
    url(r'^signup', Signup.as_view()),
    url(r'^account/', AccountPage.as_view()),
    url(r'^about', AboutPage.as_view()),
    url(r'^$', FrontPage.as_view()),
    url(r'^logout/$', logout, {'next_page': '/login/'}),
    url(r'^', include('django.contrib.auth.urls')),
    url(r'^waiting_offers/$', WaitingOffersView.as_view()),
]