from django.http import HttpResponse
from django.template import loader

from inspect import stack, getmodule
def ContextWithView(request):
    """Template context with current_view value,
    a string with the full namespaced django view in use.
    """
    # Frame 0 is the current frame
    # So assuming normal usage the frame of the view
    # calling this processor should be Frame 1
    name = getmodule(stack()[1][0]).__name__
    return {
        'current_view': "%s.%s" % (name, stack()[1][3]),
        'current_page': request.path_info.replace('/', ''),
    }


def prepare_http_response(d, request, template_name):
    d['context_instance'] = ContextWithView(request)
    d['request'] = request
    if not request.user.is_anonymous():
        d['user'] = request.user
    else:
        d['operated_lots'] = []
    t = loader.get_template(template_name)
    r = HttpResponse(t.render(d, request))
    return r
